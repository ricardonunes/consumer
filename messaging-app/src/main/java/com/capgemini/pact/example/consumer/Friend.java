package com.capgemini.pact.example.consumer;

import lombok.Data;

@Data
public class Friend {

    private String id;
    private String name;

}
