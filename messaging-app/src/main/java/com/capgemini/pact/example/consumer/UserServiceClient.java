package com.capgemini.pact.example.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class UserServiceClient {

    private final RestTemplate restTemplate;

    public UserServiceClient(@Value("${user-service.base-url}") String baseUrl) {
        this.restTemplate = new RestTemplateBuilder().rootUri(baseUrl).build();
        int i =1 ;
    }

    @GetMapping
    public User getUser(String id) {
        final User user = restTemplate.getForObject("/users/" + id, User.class);
        Assert.hasText(user.getName(), "Name is blank.");
        int i =1;
        return user;
    }

}
